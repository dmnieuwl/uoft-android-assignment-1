package com.tultechip.uoft_assignment1;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ReportActivity extends AppCompatActivity {

    // Class Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_report);

        Intent intent = getIntent();
        this.setGrade(intent.getStringExtra("ios"), R.id.iosGradeTextView);
        this.setGrade(intent.getStringExtra("android"), R.id.androidGradeTextView);
        this.setGrade(intent.getStringExtra("swift"), R.id.swiftGradeTextView);
        this.setGrade(intent.getStringExtra("java"), R.id.javaGradeTextView);

        TextView summaryTitleTextView = findViewById(R.id.summaryTitle);
        summaryTitleTextView.setText(intent.getStringExtra("calculated_name"));

        TextView summaryValueTextView = findViewById(R.id.summaryGrade);
        summaryValueTextView.setText(intent.getStringExtra("value"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setGrade(String grade, int elementId) {
        TextView textView = findViewById(elementId);
        textView.setText(grade);
    }
}
