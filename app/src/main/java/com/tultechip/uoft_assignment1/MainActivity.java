package com.tultechip.uoft_assignment1;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = "GradeCalculator";

    // Overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View view) {
        this.generateReport(view.getId());
    }


    // Private methods

    private void generateReport(int id) {
        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra("ios", this.getGrade(R.id.iosTextView));
        intent.putExtra("android", this.getGrade(R.id.androidTextView));
        intent.putExtra("java", this.getGrade(R.id.javaTextView));
        intent.putExtra("swift", this.getGrade(R.id.swiftTextView));

        switch (id) {
            case R.id.minButton:
                intent.putExtra("calculated_name", "Minimum");
                intent.putExtra("value", this.getMin());
                break;
            case R.id.averageButton:
                intent.putExtra("calculated_name", "Average");
                intent.putExtra("value", this.getAverage());
                break;
            case R.id.maxButton:
                intent.putExtra("calculated_name", "Maximum");
                intent.putExtra("value", this.getMax());
                break;
            default:
                // Unsupported
                break;
        }

        startActivity(intent);
    }

    private String getGrade(int id) {
        TextView textView = findViewById(id);
        String value = textView.getText().toString();
        if (value.equals("")) {
            value = "0";
        }
        return value;
    }

    private void setGrade(int id, String value) {
        TextView textView = findViewById(id);
        textView.setText(value);
    }

    @org.jetbrains.annotations.NotNull
    private String getAverage() {
        double total = 0.0;
        total += Double.parseDouble(this.getGrade(R.id.iosTextView));
        total += Double.parseDouble(this.getGrade(R.id.androidTextView));
        total += Double.parseDouble(this.getGrade(R.id.javaTextView));
        total += Double.parseDouble(this.getGrade(R.id.swiftTextView));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return decimalFormat.format(total / 4.0);
    }

    private String getMin() {
        double minValue = Double.MAX_VALUE;
        minValue = Math.min(minValue, Double.parseDouble(this.getGrade(R.id.iosTextView)));
        minValue = Math.min(minValue, Double.parseDouble(this.getGrade(R.id.androidTextView)));
        minValue = Math.min(minValue, Double.parseDouble(this.getGrade(R.id.javaTextView)));
        minValue = Math.min(minValue, Double.parseDouble(this.getGrade(R.id.swiftTextView)));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return decimalFormat.format(minValue);
    }

    private String getMax() {
        double maxValue = Double.MIN_VALUE;
        maxValue = Math.max(maxValue, Double.parseDouble(this.getGrade(R.id.iosTextView)));
        maxValue = Math.max(maxValue, Double.parseDouble(this.getGrade(R.id.androidTextView)));
        maxValue = Math.max(maxValue, Double.parseDouble(this.getGrade(R.id.javaTextView)));
        maxValue = Math.max(maxValue, Double.parseDouble(this.getGrade(R.id.swiftTextView)));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return decimalFormat.format(maxValue);
    }
}
